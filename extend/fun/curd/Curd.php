<?php
/**
 * FunAdmin
 * ============================================================================
 * 版权所有 2017-202InputOption::VALUE_IS_ARRAY FunAdmin，并保留所有权利。
 * 网站地址: https://www.FunAdmin.com
 * ----------------------------------------------------------------------------
 * 采用最新Thinkphp6实现
 * ============================================================================
 * Author: yuege
 * Date: 2020/9/21
 */

namespace fun\curd;

use fun\curd\service\CurdService;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;


/**
 * Class Curd
 * @package app\backend\command
 * 功能待完善
 */
class Curd extends Command
{
    protected static $defaultName = 'curd';
    protected static $defaultDescription = 'Curd Command';
    protected function configure()
    {
        $this->setName('curd')
            ->addOption('driver', '', InputOption::VALUE_OPTIONAL, '数据库', 'mysql')
            ->addOption('table', 't', InputOption::VALUE_REQUIRED, '表名', null)
            ->addOption('controller', 'c', InputOption::VALUE_OPTIONAL, '控制器名', null)
            ->addOption('model', 'm', InputOption::VALUE_OPTIONAL, '模型名', null)
            ->addOption('fields', 'i', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '显示字段', null)
            ->addOption('validate', 'l', InputOption::VALUE_OPTIONAL, '验证器', null)
            ->addOption('joinTable', 'j', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '关联表名', null)
            ->addOption('joinModel', 'o', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '关联模型', null)
            ->addOption('joinName', 'e', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '关联模型名字', null)
            ->addOption('joinMethod', 'w', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '关系表方式 hasone or belongsto等', null)
            ->addOption('joinPrimaryKey', 'p', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '关联主键', null)
            ->addOption('joinForeignKey', 'k', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '关联外键', null)
            ->addOption('joinFields', 's', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '关联表显示字段', null)
            ->addOption('selectFields', '', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '关联表下拉显示字段', null)
            ->addOption('imageSuffix', '', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '图片后缀', null)
            ->addOption('fileSuffix', '', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '文件后缀', null)
            ->addOption('timeSuffix', '', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '时间后缀', null)
            ->addOption('switchSuffix', '', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '开关后缀', null)
            ->addOption('radioSuffix', '', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '单选后缀', null)
            ->addOption('checkboxSuffix', '', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '多选后缀', null)
            ->addOption('citySuffix', '', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '城市后缀', null)
            ->addOption('iconSuffix', '', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '图标后缀', null)
            ->addOption('jsonSuffix', '', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'json后缀', null)
            ->addOption('selectSuffix', '', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '下拉选择后缀', null)
            ->addOption('selectsSuffix', '', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '下拉多选后缀', null)
            ->addOption('sortField', '', InputOption::VALUE_OPTIONAL, '排序字段', null)
            ->addOption('statusField', '', InputOption::VALUE_OPTIONAL, '状态字段', null)
            ->addOption('ignoreFields', 'g', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, '忽略的字段', null)
            ->addOption('method', '', InputOption::VALUE_OPTIONAL, '方法', null)
            ->addOption('page', '', InputOption::VALUE_OPTIONAL, '是否页', null)
            ->addOption('limit', '', InputOption::VALUE_OPTIONAL , '分页大小', null)
            ->addOption('addon', 'a', InputOption::VALUE_OPTIONAL, '插件名', '')
            ->addOption('menu', 'u', InputOption::VALUE_OPTIONAL, '菜单', 0)
            ->addOption('force', 'f', InputOption::VALUE_OPTIONAL, '强制覆盖或删除', 0)
            ->addOption('delete', 'd', InputOption::VALUE_OPTIONAL, '删除', 0)
            ->addOption('jump', '', InputOption::VALUE_OPTIONAL, '跳过重复文件', 1)
            ->addOption('app', '', InputOption::VALUE_OPTIONAL, '是否是APP', 'backend') //暂时无效
            ->setDescription('Curd Command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new  SymfonyStyle($input,$output);
        $param = [];
        $param['driver'] = $input->getOption('driver');
        $param['table'] = $input->getOption('table');
        $param['controller'] = $input->getOption('controller');
        $param['page'] = $input->getOption('page');
        $param['limit'] = $input->getOption('limit');
        $param['model'] = $input->getOption('model');
        $param['validate']  = $input->getOption('validate');
        $param['method']  = $input->getOption('method');
        $param['addon'] = $input->getOption('addon');        //区块 。插件名字
        $param['fields'] = $input->getOption('fields');//自定义显示字段
        $param['checkboxSuffix'] = $input->getOption('checkboxSuffix');
        $param['radioSuffix'] = $input->getOption('radioSuffix');
        $param['imageSuffix'] = $input->getOption('imageSuffix');
        $param['fileSuffix'] = $input->getOption('fileSuffix');
        $param['timeSuffix'] = $input->getOption('timeSuffix');
        $param['iconSuffix'] = $input->getOption('iconSuffix');
        $param['switchSuffix'] = $input->getOption('switchSuffix');
        $param['selectsSuffix'] = $input->getOption('selectsSuffix');
        $param['sortField'] = $input->getOption('sortField');
        $param['ignoreFields'] = $input->getOption('ignoreFields');
        $param['joinTable'] = $input->getOption('joinTable');
        $param['joinName'] = $input->getOption('joinName');
        $param['joinModel'] = $input->getOption('joinModel');
        $param['joinMethod'] = $input->getOption('joinMethod');
        $param['joinForeignKey'] = $input->getOption('joinForeignKey');
        $param['joinPrimaryKey'] = $input->getOption('joinPrimaryKey');
        $param['selectFields'] = $input->getOption('selectFields');
        $param['force'] = $input->getOption('force');//强制覆盖或删除
        $param['delete'] = $input->getOption('delete');
        $param['menu'] = $input->getOption('menu');
        $param['jump'] = $input->getOption('jump');
        $param['app'] = $input->getOption('app');
        if (empty($param['table'])) {
            $output->writeln("主表不能为空");
            return self::SUCCESS;
        }
        $curdService = new CurdService($param);
        try {
            $curdService->maker();
            $io->success('make success');
        }catch (\Exception $e){
            $io->writeln('----------------');
            $io->error($e->getMessage());
            $output->writeln('----------------');
            return self::FAILURE;
        }
        return self::SUCCESS;
    }
}
