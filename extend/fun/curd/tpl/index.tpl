    /**
    * @NodeAnnotation(title="List")
    */
    public function index()
    {
        {{$relationSearch}}
        if (request()->isAjax()) {
            if (request()->input('selectFields')) {
                $this->selectList();
            }
            list($this->page, $this->pageSize,$sort,$where) = $this->buildParames();
            $list = $this->modelClass
                ->where($where)
                ->{{$joinIndexMethod}}
                ->order($sort)
                ->paginate([
                    'list_rows'=> $this->pageSize,
                    'page' => $this->page,
                ]);
            $result = ['code' => 0, 'msg' => lang('Get Data Success'), 'data' => $list->items(), 'count' =>$list->total()];
            return json($result);
        }
        return fetch();
    }

